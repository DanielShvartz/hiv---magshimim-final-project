import bluetooth

"""
This code is only to preview of could find a device
-------------------------------------------------------
This only works if:
bluetooth on the computer and phone is on.
phone visibility is on - so we can detect the phone
"""

target_name = input("Enter your phone bluetooth name to connect to: ")
target_address = None

print("Scanning...")
nearby_devices = bluetooth.discover_devices(duration=15, lookup_names = True)
#discover_devices runs for 10 seconds or given amount of time, gets all the addresses and adds them with
#the address name as a tuple into a list
print("Nearby devices = " ,nearby_devices)

for device in nearby_devices: # for each tuple in the list
    print("Device", device) # prints its name
    if target_name == device[1]: # if the name is as the wanted
        target_address = device[0] # save the address and stop searching
        break

if target_address is not None: # if we have tjhe address
    print("found target bluetooth device with address ", target_address) # print it
else:
    print("could not find target bluetooth device nearby")

